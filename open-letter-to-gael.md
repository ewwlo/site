---
layout: default
permalink: /open-letter-to-gael
---

# An open letter to Gaël Duval, from /e/vil Founder

Dear Gaël Duval,

I'm certain that at /e/ you want to achieve perfect privacy for everyone.

HOWEVER, you seem to have forgotten a few facts about /e/ & eOS:

1. eOS is mostly [a pre-built system](/evil.html#they-use-pre-built-applications-possibly-malware). This means that users (well, software specialists actually in this case) can only trust your claims, instead of being able to verify that the technologies that you are putting in place are safe and guarantee your users against all privacy threats.

2. The /e/ Foundation [steals code](/evil.html#they-steal-code). This means that, not even with those $200k of yours, you can't properly credit open source contributors.

3. The moderation of your Telegram chat [doesn't accept negative feedback](/evil.html#they-dont-accept-negative-feedback). This means that us, users, can't explain why a lot of the "/e/ ecosystem" is stupid and should be redesigned.

At [NoGooLag](https://telegram.me/NoGooLag), we're making a informal place to talk about truly private private alternatives to Google, the proper way. Putting privacy over user friendlyness.

We think that Open Source is the only way to bring verifiable privacy, as opposed to simply claims and reputation.

We also think that an attractive mobile ecosystem worth of $200k should be *at least* not a fucking fork of LineageOS.

Let's talk `v0id at riseup dot net`.

Sincerely yours,

v0idifier, [/e/vil](/evil.html) Founder.

Follow me on [the fediverse](https://todon.nl/@v0idifier) or [message me on Telegram](https://telegram.me/v0idifier).
