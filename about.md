---
layout: default
permalink: /about
---

# About ewwlo

We are an **independent**, grassroots group investigating many of [eelo](https://e.foundation)'s scams.
We are not paid by any other project, organization or related; our research is done at home, without anyone in the middle.
We try to expose the damage that eelo's doing to the Android community and related.

Anyone can help. Join our [telegram group chat](https://t.me/ewwlo) for news, info, etc.
