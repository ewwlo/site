---
layout: default
---
<!--<meta property="og:title" content="An /e/wwlo winter update: bullshit, gaël triggered & next steps">
<meta property="og:description" content="Hello sur! You want privacy on your SmartPhone??? then DON'T GET eelO! /e/elo copies code and does other scummy stuffs.">
<meta property="og:type" content="article">
<meta property="og:url" content="https://ewwlo.xyz/2019-winter-update">-->

# An /e/wwlo winter update: bullshit, gaël triggered & next steps
**last updated: 25/07/2019 - authors:**
 * [~void](https://void.partidopirata.com.ar)
 * [Thespartann](https://telegram.me/Thespartann)

![A cute cat picture](images/qt_cat_pic.jpg)

Hello sur! You want privacy on your SmartPhone??? then DON'T GET eelO!

This is an analysis of [this article](https://medium.com/@gael_duval/an-e-summer-update-smartphones-for-sale-applications-pwas-next-steps-85e0621e9ec6)
made by the $eelo foundation$, plus some actual updates about our project. To clarify,
the title says "winter update" because here in Argentina it's winter. I don't care what
the USA says. Anyway, let's get started.

> At the end of 2017, the /e/ project was unveiled in a series of articles [...] with a clear promise: making a fully unGoogled and ready-to-use mobile operating system and online services, yet compatible with Android applications.
> Eighteen months later, most of what was described in that early vision has been built. We have produced an unGoogled mobile OS (currently supported on 80 different smartphone models) and /e/ associated online services — email address, online storage, calendar & notes — are up and running. All accessed through a single personal /e/ identity and password (read a comprehensive description here).

Okaaay, this is misleading. First of all, the "unGoogled mobile OS" is a
fork of [LineageOS](https://lineageos.org/) that isn't even updated (they
run Android version 8 (Oreo), even though 10 is about to come out.) This
"OS" has been specifically criticised (?) over at [/e/vil](/evil.html).

Then it goes and says it supports over "80 different smartphones models".
Again, misleading. This sounds like they took the time to individually
support each device, but the truth is that all of these supported devices
were already supported by LineageOS, so they just used their work and
basically claimed that they made it (check /e/vil, where they specifically
do this to other parts of LineageOS.)

And then it talks about the "online services". The thing is, all/most of
these online services are hosted on the server hosting provider [Scaleway](https://www.scaleway.com/en/),
which isn't exactly a private hosting provider like [Njalla](https://njal.la).

But that's not the main issue. Truly private online services would encrypt
your files/calendar/contacts/whatever on your device, and then upload it.
/e/elo's online services don't do this. You are trusting /e/elo, and
Scaleway, that they aren't looking and selling your info to the NSA or
whatever. *This is not how private online services look like.*

## The /e/elo tax

> Still more recently, we have introduced smartphones for purchase with /e/OS preloaded: a selection of refurbished premium grade hardware options (Galaxy S7, S9…) are now available, [...]

So basically, they buy phones, put /e/OS on it, put it in a fancy box and
sell them for more money (the /e/elo tax™).

Well, we kindly took the time to make a price comparison between the
refurbished phones and the /e/elo phones (the /e/elo tax™). Here it is:

| Phone | Amazon price | /e/elo price | *tax* |
|-------|--------------|--------------|-----|
| Samsung S7 | [130$](https://www.amazon.com/Samsung-G930V-Smartphones-Certified-Refurbished/dp/B01HTZ4T8C/ref=sr_1_2?keywords=Samsung+s7&qid=1563965923&s=gateway&sr=8-2) | 280$ | *150$* |
| Samsung S7 Edge | [220$](https://www.amazon.com/Samsung-Unlocked-Titanium-Certified-Refurbished/dp/B01M7O431L/ref=sr_1_2?keywords=Samsung+s7+edge&qid=1563965987&s=gateway&sr=8-2)| 300$ | *80$* |
| Samsung S9 | [370$](https://www.amazon.com/Samsung-Galaxy-Unlocked-Certified-Refurbished/dp/B07C65XFBB/ref=sr_1_4?keywords=Samsung+S9&qid=1563966047&s=gateway&sr=8-4) | 450$ | *80$* |
| Samsung S9+ | [390$](https://www.amazon.com/Samsung-Galaxy-Unlocked-Certified-Refurbished/dp/B07C65VV3R/ref=sr_1_4?keywords=Samsung+S9%2B&qid=1563966120&s=gateway&sr=8-4)| 480$ | *90$* |
| Average |  |  | **100$** |

That's right folks, the average /e/elo tax is **100$USD**.

> It seems we have invented a new concept here, with demand exceeding our expectations.

No, you haven't invented anything. [CopperheadOS](https://copperhead.co/)
has been selling secure phones (with CopperheadOS AND proper security
measures), which is exactly what you are doing but better.

Oh, and weren't you supposed to be a non-profit organization? That's what
y'all got advertised like on your 200k$EUR Kickstarter/IndieGoGo campaign.

## More bull$hit

> We are proud to anchor /e/ on positive values such as user data privacy, education, energy saving and sustainable development.

 * data privacy: proved fake in this article and /e/vil
 * education: literally none
 * energy saving: i guess they say this because not having Google Play
   services can save a lot of energy. this can be achieved by just using
   a normal [LineageOS for MicroG](https://lineage.microg.org/)
   installation. plus, adding so many bloat apps into the OS doesn't help.
 * sustainable development: stealing code from LineageOS and Aurora Store
   is pretty sustainable, sure.

> What we are building today is not perfect yet, but we believe that it’s the beginning of a truly significant alternative to the Apple-Google duopoly in the smartphone market. And we are getting a strong support for this from our fast-growing community of users.

So we make the /e/elo monopoly! Everyone else spies on you. /e/elo is
definatly the only safe option.

## The app repo

Sadly, we had to release this piece quick so we haven't investigated about
this yet. Be prepared, though. And help us out on our [telegram group](https://t.me/ewwlo) :).

## PWAs

> The goal of /e/ ultimately is to get totally free from the Google and the Android applications ecosystem. It’s not totally possible yet, and will take time, perhaps several years. Progressive Web Apps are maturing fast as a new option for developing low-cost efficient mobile applications, and we want to catch this opportunity and be one of the first actors to embrace it.

PWAs are great, but they are definatly not efficient on many ways, mainly
in RAM consumption. You need at least 3GB of RAM on your device to
fluidly run PWA apps like native apps, and phones with this specifications
are from the mid-high end right now. Oh, and they are bad on CPU usage,
so forget about "energy saving".

## Even more bull$hit

> Users demand something new and better. We’re in an emerging market — fast growing and very dynamic. We believe that what we are doing is starting to answer real needs, and we are committed to delivering more value and privacy over time.

I would like to end this section of the article by saying that what /e/elo
causes is a fake illusion of privacy. Mainly the online services are the
biggest illusion. They are not private. They are not safe.

## On Gaël (/e/elo founder) getting triggered

Here's some funny screenshots of Gaël making fun of himself.

![Gaël post on Mastodon: "I know this shit"](images/gael_post_shit.jpg)
![Gaël post on Mastodon: "all information about our project is public, facts can be verified. After that, people can hide behind their screens and try to propagate lies. I don't have time to play that game"](images/gael_post_lies.jpg)
![Gaël post on Mastodon: "you can believe to fake news, that's your choice. However haters remain haters and I have  no time for hate, life is too short. Good evening."](images/gael_post_fake_news.jpg)

[post 1](https://mastodon.social/@gael/102112211649018394)
[post 2](https://mastodon.social/@gael/102112345423353738)
[post 3](https://mastodon.social/@gael/102112549067931534)

Hey Gaël, if our website is so fake, why don't you contact us to fix it?
My Telegram is [@v0idifier](https://t.me/v0idifier), my current Mastodon
is [@v0idifier@todon.nl](https://todon.nl/@v0idifier) (as you know,
because you blocked it) and my email is void (with a zero instead of o)
at riseup dot net. Please go ahead and debunk our website, because
clearly you haven't yet.

## Our next steps

We will keep analyzing their code, "OS" and services and we will keep
watching /e/elo.

There's much more to do. /e/elo has deleted our comments on their
forums. And there's a lot to investigate. Join us!

## Your next steps

Don't use /e/elo, and don't donate to them, and don't buy anything
from them.

Thanks for reading! Join our [Telegram group](https://t.me/ewwlo) for
anti-/e/elo news (?) and to help us keep making this content!
